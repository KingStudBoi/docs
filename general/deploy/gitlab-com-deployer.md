## Overview

GitLab.com deployments are initiated by GitLab Chatops, this triggers
a new pipeline on the [Deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer) project.

To see all available options for deploy run `/chatops run deploy --help`:

## Creating a new deployment for _upgrading_ GitLab

Deployments are initiated using GitLab Chatops.
For example, to initiate a deployment of 11.8.0 to **staging**:

```
/chatops run deploy 11.8.0.ee.0
```

## Override variables

Variables can be set on the deployer pipeline to change its behavior or
the jobs that will be executed.  See [variables.md](../../runbooks/variables.md)

### Bypassing Failures

#### Skipping Production Promotion Checks

In cases where there may be an incident or a change that would block a deploy,
perform the necessary investigation to determine if it is safe to proceed with a
deploy.  We can then utilize the option `ignore-production-checks` with a reason
indicating why these checks are skipped.  The reason is placed into the release
issue for audit purposes.

**Make sure to let the EOC add a response to the "..started a deployment" comment on the release issue to record their approval of the override. We need
this for compliance.**

Example command:

```
/chatops run deploy 11.8.0.ee.0 --production --ignore-production-checks 'Insert a reason for
skipping production checks here'
```

This essentially sends the variable `IGNORE_PRODUCTION_CHECKS` to the deployment
pipeline.

#### Skipping Canary Promotion Checks

In case a manual deploy to canary is required, the following command can be used:

```
/chatops run deploy 13.6.202011122020-fe7dcb0a4ee.886b1d4c02a --canary --production
```

#### Skipping Prepare Job Failures

The CI Job `<ENV>-prepare` may fail if a node is down in haproxy.  We can bypass
the forced failure on the prepare job by using the option
`allow-precheck-failure`.  Example:

```
/chatops run deploy 11.8.0.ee.0 --allow-precheck-failure
```

This essentially sends the variable `PRECHECK_IGNORE_ERRORS` to the deployment
pipeline.

### Creating a new deployment for _rolling back_ GitLab

**IMPORTANT: Before initiating a rollback ensure to follow the [appropriate runbook](../../runbooks/rollback-a-deployment.md),
the following paragraph covers how to start a manual rollback pipeline on the deployer project in case of a chatops failure.**

In case of need it's possible to start a deployer pipeline by initiating a new pipeline
on the [deployer project](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/new)
**default branch** and setting the environment and version CI variables.

* `DEPLOY_ENVIRONMENT`: **required** (i.e.: `gstg`, `gprd`)
* `DEPLOY_VERSION`: **required**
* `DEPLOY_ROLLBACK`: **required for rollback**, set to `true` to initiate a rollback pipeline

_Note that all pipelines initiated on branches will automatically set the
`--dry-run` flag which can be useful for testing._

![Deployer Pipeline](../images/deployer-example.png)


### CICD Pipeline for a Deployer

#### Assets

Assets are either extracted from the assets docker image if it is available or pulled from the omnibus package.
This is done with a [shell script](https://gitlab.com/gitlab-com/gl-infra/deploy-tooling/-/blob/master/bin/fetch-and-upload-assets) in the deploy pipeline at the same time when we run database migrations in a job called `<ENV>-assets.  See [.gitlab-ci](https://gitlab.com/gitlab-com/gl-infra/deployer/-/blob/d0b25b4c783d6010a9d7d0ce64ae49cdda813bb6/.gitlab-ci.yml#L345-359).
After extraction they are upload to an object storage bucket which serves as the origin for our asset CDN.
It is assumed that all assets have hashed filenames so a long cache lifetime is set `Cache-Control:public,max-age=31536000`.

```mermaid
graph TB;
    w[web browser] --- a[Fastly CDN<br/>assets.gitlab-static.net];
    w --- c[CloudFlare CDN<br/>gitlab.com];
    a ---|/assets| b[Object Storage];



    c ---|/*| d[HAProxy];
    c ---|/assets| d[HAProxy];

    a ---|/*| c;
    d ---|/assets| b;
    subgraph fleet
      d --- v[VM Infrastructure];
      d --- k[K8s Infrastructure];
    end
```

When the browser requests an asset under `/assets`, it will either be for `assets.gitlab-static.net/assets` or `gitlab.com/assets`:
* If the request is for `assets.gitlab-static.net/assets` it will arrive at the Fastly CDN, which is configured to use a GCS object storage bucket as an origin for all requests to `/assets`.
* If the request is for `gitlab.com/assets`, the request will go to the CloudFlare CDN, then HAProxy, which proxies to object storage.

There is an outstanding issue to simplify this by removing the Fastly CDN https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11411

#### Upgrade Pipeline (default)

The following stages are run for upgrade for a single environment upgrade:

```mermaid
graph LR;
    a>prepare] ==> b>migrations and assets];
    b ==> c>gitaly deploy];

    subgraph fleet;
      c -.- d>sidekiq];
      c -.- d1>git];
      c -.- d2>web];
      c -.- d3>api];
      c -.- d4>pages];
      c -.- d5>kubernetes];

      subgraph k8s-workloads/gitlab-com
        d5 -.-|trigger| sidekiq
      end
    end

    sidekiq ==> e>postdeploy migrations];
    e ==> f>cleanup];
    f ==> g>gitlab-qa];
```

#### Upgrade Pipeline (multi-environment)

Multiple environments can be passed into the deployer project to chain
deployments, this works by setting `DEPLOY_ENVIRONMENT` to a comma separated
list of environments. The following chained environments are supported:

* `DEPLOY_ENVIRONMENT=gstg,gprd-cny`: Staging -> Production canary
* `DEPLOY_ENVIRONMENT=gstg,gprd-cny,gprd`: Staging -> Production canary -> Production

A deployment pipeline from staging -> production as the following stages that
provide safety QA checks and a manual promotion to production:

```mermaid
graph LR;
    a>Staging deploy] ==> b>Staging QA smoke tests];
    b ==> c>Production canary deploy];
    c ==> d>Canary smoke tests];
    d ==> e>Manaul approval];
    e ==> f>Production Deploy];
```


#### Change-lock periods for deployment

Before deploying to stage that receives production traffic there is an additional
check for a change-lock periods which are configured in a
[configuration file](https://gitlab.com/gitlab-com/gl-infra/change-lock/blob/master/config/changelock.yml)
in the [change-lock project](https://gitlab.com/gitlab-com/gl-infra/change-lock). This is not yet a product feature
https://gitlab.com/gitlab-org/gitlab-ce/issues/51738 so the deployer pipeline
uses script published in a docker image that is run before the production stages
of deployment.

This yaml file specified one more windows where the production jobs will fail,
it uses a cron or date syntax for determining the change-lock period.

For more information about specifying change-lock periods, see the [project README](https://gitlab.com/gitlab-com/gl-infra/change-lock/blob/master/README.md).

**Note**: The change-lock period only runs for automated deployments, where
the `DEPLOY_USER=deployer`. For a normal chatops based deployment the user
will be set to the name of the individual who initiated the deploy.

#### CICD Pipeline for a Deployer Rollback

A rollback pipeline has the same stages, except that the `gitaly`
stage is placed after the `fleet` stage. This is necessary because
it's possible that rails will have changes that are incompatible with
earlier versions of Gitaly.

```mermaid
graph LR;
    a>prepare] ==> c>migrations];
    subgraph fleet;
      c -.- d>sidekiq];
      c -.- d1>git];
      c -.- d2>web];
      c -.- d3>api];
      c -.- d4>pages];
      c -.- d5>kubernetes];

      subgraph k8s-workloads/gitlab-com
        d5 -.-|trigger| sidekiq
      end
    end

    sidekiq ==> b>gitaly deploy];
    b ==> e>postdeploy migrations];
    e ==> f>cleanup];
    f ==> g>gitlab-qa];
style b fill:#E0FFFF;
```

#### Rollback considerations for database migrations

* Before initiating a rollback, background migrations should be evaluated. The
  DBRE oncall should asses the impact of rolling back. Note that clearing
  the background migration queue may not be the best course of action as these
  migrations should be backwards compatible with previous application versions.
  For information on how to clear queues see
  [the sidekiq troubleshooting guide](https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/large-sidekiq-queue.md#viewing-and-killing-jobs-from-the-queue)
* If the current version introduced one or more post-deployment migrations,
  these migrations must be reverted before rolling back the code changes.
  This is a manual process and should be assessed by
  a DBRE before the rollback is initiated.
  https://gitlab.com/gitlab-org/release/framework/issues/234 discusses
  how to we can better deal with post-deploy migrations in the context of
  rollbacks.
* Regular migrations _can be reverted_ but **they are not** reverted in
  the rollback pipeline. Migrations are designed to be backwards compatible
  with the previous version of application code.
* Rolling back more than one version without a very thorough review should never
  be attempted.

## Upgrade / Rollback Deploy Stages

* **prepare**: The prepare stage is a single job that is responsible for all
  non-destructive changes before a deployment. This is the general place we put
  checks and notifications before continuing with a deployment. The following
  tasks are executed in the prepare stage:
  * slack notifications
  * notes the start time of the deploy
  * checks to see if the package is indexed in packagecloud
  * verifies haproxy status
  * verifies that there are no critical alerts
  * verifies the version is consistent across the fleet

* **migrations**: This stage runs online database migrations for staging and canary deployments.
  We do not run online migrations for production deployments because they are handled
  by the canary stage

* **gitaly deploy**: The Gitaly deploy happens before the rest of the fleet in case there are rails
  changes in the version being deployed that take advantage of new Gitaly features. If the Gitaly version
  is not changed, the omnibus package update is skipped, and updated later
  when the chef is run after the pipeline completes.  If the Gitaly version
  is updated, a Gitaly restart is issued which will often result in errors on
  GitLab.com.

* **fleet deploys**: The rest of the fleet is deployed to in parallel, the logic
  is identical for each set of roles. The omnibus is upgraded, gitlab-ctl reconfigure is
  run and if the service version changes a restart is issued. In the case of rails
  we always issue a HUP instead of a restart.

  This stage also includes the necessary job to trigger an upgrade to our
  Kubernetes infrastructure.  We can read more about this in section [Kubernetes
  Upgrade](#kubernetes-upgrade)

* **postdeploy migrations**: [Post-deploy migrations](https://docs.gitlab.com/ee/development/post_deployment_migrations.html#post-deployment-migrations)
  are run last and may be a
  point-of-no-return for upgrades as it might make a change that is not
  backwards compatible with previous versions of the application.

* **cleanup**: Like _prepare_, the cleanup stage is a single job that handles
  any post-install tasks that need to run at the end of deployment. This
  includes slack notifications, grafana annotations and starting Chef across the
  fleet.

* **gitlab-qa**: The very last stage of the pipeline runs a set of QA smoke tests
  against an environment on which the deploy is running.

### Omnibus Installation

The omnibus installation is handled by a [task file](https://gitlab.com/gitlab-com/gl-infra/deploy-tooling/blob/master/common_tasks/install_gitlab_ee.yml)
that has the following steps:

```mermaid
graph LR;
    a>stop chef] ==> b>rollback any existing patches];
    b ==> c>install gitlab-ee];
    c ==> d>apply any new patches];
    d ==> e>run gitlab-ctl reconfigure];
    e ==> f>restart services if necessary];
```

Note that existing patches are rolled back and new patches are applied on every
deploy. Deployer does this by referencing the [patcher repository push mirror](https://ops.gitlab.net/gitlab-com/gl-infra/patcher)
as a submodule for applying post-deployment-patches during deployments.

For more information about how patches are applied to the fleet see the
[GitLab post-deployment patcher](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)
documentation.

### Kubernetes Upgrade

Inside of deployer, we have a helper script
[cng-versions-from-deploy-version](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/blob/master/bin/cng-versions-from-deploy-version)
to help match the variety of ways that our deployment versions come into
deployer and translate these into the correct format that is required by our
GitLab Helm chart.  These properly formatted versions are sent to downstream
pipelines as new environment variables.  Please view the comments in the script
for details.

Also inside of deployer, is a job to check and validate that the desired
deployment images have been successfully built in our CNG project.  This is to
ensure we do not accidentally perform a deploy while missing essential
components to our infrastructure.  This is play
[wait_cng_build](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/blob/master/wait_cng_build.yml)

Deployer will then trigger a pipeline to the k8s-workloads/gitlab-com project.
The job responsible for the trigger is called `<ENV>-kubernetes` in deployer.
This trigger sends all the variables for the Deployment.  The trigger can be
seen in our deployer
[.gitlab-ci.yml](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/blob/1a672d998c06c8db2a10faffa6e01acbb44a4505/.gitlab-ci.yml#L316)

In k8s-workloads/gitlab-com, we have a special set of CI jobs specific for
auto-deploy. These can be see in our
[.gitlab-ci.yml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/f813c424579b79eed45a1206001d3d0ee6e1a270/.gitlab-ci.yml#L237)
file. These jobs will only operate on the specific environment passed into the
triggered pipeilne.  At most a triggered pipeline will have two jobs:

```mermaid
graph LR;
  a>dry-run] ==> b>auto-deploy];
```

The dry-run job is there to catch errors prior to the auto-deploy job.  The
auto-deploy job will proceed automatically and perform the necessary upgrade to
the Kubernetes infrastructure.

### Repositories overview

```mermaid
graph TD
    subgraph patcher;
    a1[gitlab-ci.yml];
    a2[deploy-tooling submodule];
    end;
    subgraph deployer;
    b1[gitlab-ci.yml];
    b2[deploy-tooling submodule];
    b3[patcher submodule];
    b4[k8s-workloads/gitlab-com trigger];
    end;
```
* [gl-infra/deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/) is the repository where the CICD configuration is maintained
  for defining the pipeline. It is sourced on https://ops.gitlab.net with a public
  mirror on https://gitlab.com.
* [gl-infra/patcher for engineering](https://ops.gitlab.net/engineering/patcher/) is the repository where post-deployment patches live and can be accessed by all of engineering.
* [gl-infra/patcher push mirror](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/) is the repository where post-deployment patcher pipelines are run it is a private repo that can only be accessed by SREs.
* [gl-infra/deploy-tooling](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/) is a common repository that is used as a submodule
  for all other repos that require Ansible code. This repository contains the
  plays, plugins and scripts for deployment. It is sourced on
  https://ops.gitlab.net with a public mirror on https://gitlab.com.
* [k8s-workloads/gitlab-com](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/)
  is the repository that contains all the necessary components for GitLab.com
  that operate on Kubernetes Infrastructure.
